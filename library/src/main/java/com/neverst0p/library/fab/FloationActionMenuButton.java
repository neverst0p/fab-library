package com.neverst0p.library.fab;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.IntDef;
import android.support.design.widget.FloatingActionButton;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * TODO: document your custom view class.
 */
public class FloationActionMenuButton extends ViewGroup {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({OPEN_UP, OPEN_DOWN, OPEN_LEFT, OPEN_RIGHT})
    public @interface FabMenuOpenDirection {}

    public static final int OPEN_UP = 0;
    public static final int OPEN_DOWN = 1;
    public static final int OPEN_LEFT = 2;
    public static final int OPEN_RIGHT = 3;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({LABELS_LEFT, LABELS_RIGHT})
    public @interface FabLabelsPosition {}

    public static final int LABELS_LEFT = 0;
    public static final int LABELS_RIGHT = 1;

    private static final int ANIMATION_DURATION = 300;
    private static final float CLOSE_WITH_ROTATION = 0f;
    private static final float OPEN_WITH_ROTATION_CCW = 90f + 45f;
    private static final float OPEN_WITH_ROTATION_CW = -90f - 45f;

    /** Fab Menu Button */
    private FloatingActionButton mMenuButton;

    private int mFabMenuButtonSize;
    private int mMaxFabMenuButtonWidth;
    private int mMaxFabMenuButtonHeight;

    @ViewDebug.ExportedProperty(category = "layout", mapping = {
            @ViewDebug.IntToString(from = OPEN_UP, to = "UP"),
            @ViewDebug.IntToString(from = OPEN_DOWN, to = "DOWN"),
            @ViewDebug.IntToString(from = OPEN_LEFT, to = "LEFT"),
            @ViewDebug.IntToString(from = OPEN_RIGHT, to = "RIGHT")
    })
    private int mFabMenuOpenDirection = OPEN_UP;

    @ViewDebug.ExportedProperty(category = "layout")
    private int mFabMenuItemButtonSpacing;

    private boolean mFabMenuOpened;

    private int mFabMenuItemButtonsCount;

    private boolean mFabMenuLabelEnabled;
    private String mFabMenuLabel;
    private int mFabMenuLabelStyle;

    /** Fab Menu Item Labels */
    @ViewDebug.ExportedProperty(category = "layout")
    private int mFabMenuItemLabelsMargin;

    @ViewDebug.ExportedProperty(category = "layout")
    private int mFabMenuItemLabelsVerticalOffset;
    private int mFabMenuItemLabelsStyle;

    @ViewDebug.ExportedProperty(category = "layout", mapping = {
            @ViewDebug.IntToString(from = LABELS_LEFT, to = "LEFT"),
            @ViewDebug.IntToString(from = LABELS_RIGHT, to = "RIGHT")
    })
    private int mFabMenuItemLabelsPosition = LABELS_LEFT;

    /** Fab Menu Background */
    private ValueAnimator mBackgroundShowAnimation;
    private ValueAnimator mBackgroundHideAnimation;
    private int mBackgroundColor;


    /** Fab Menu Animations */
    private AnimatorSet mOpenAnimation = new AnimatorSet().setDuration(ANIMATION_DURATION);
    private AnimatorSet mCloseAnimation = new AnimatorSet().setDuration(ANIMATION_DURATION);

    /** Fab Menu Toggle Listeners */
    private OnFabMenuToggleListener mListener;

    //private RotatingDrawable mRotatingDrawable;



    //private TouchDelegateGroup mTouchDelegateGroup;

    private interface OnFabMenuToggleListener {
        void onMenuToggle(boolean opened);
    }

    private String mExampleString; // TODO: use a default from R.string...
    private int mExampleColor = Color.RED; // TODO: use a default from R.color...
    private float mExampleDimension = 0; // TODO: use a default from R.dimen...
    private Drawable mExampleDrawable;



    private TextPaint mTextPaint;
    private float mTextWidth;
    private float mTextHeight;

    public FloationActionMenuButton(Context context) {
        super(context);
        init(null, 0);
    }

    public FloationActionMenuButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public FloationActionMenuButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.FloationActionMenuButton, defStyle, 0);

        mExampleString = a.getString(
                R.styleable.FloationActionMenuButton_menuExampleString);
        mExampleColor = a.getColor(
                R.styleable.FloationActionMenuButton_menuExampleColor,
                mExampleColor);
        // Use getDimensionPixelSize or getDimensionPixelOffset when dealing with
        // values that should fall on pixel boundaries.
        mExampleDimension = a.getDimension(
                R.styleable.FloationActionMenuButton_menuExampleDimension,
                mExampleDimension);

        if (a.hasValue(R.styleable.FloationActionMenuButton_menuExampleDrawable)) {
            mExampleDrawable = a.getDrawable(
                    R.styleable.FloationActionMenuButton_menuExampleDrawable);
            mExampleDrawable.setCallback(this);
        }

        a.recycle();

        // Set up a default TextPaint object
        mTextPaint = new TextPaint();
        mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextAlign(Paint.Align.LEFT);

        // Update TextPaint and text measurements from attributes
        invalidateTextPaintAndMeasurements();
    }

    private void invalidateTextPaintAndMeasurements() {
        mTextPaint.setTextSize(mExampleDimension);
        mTextPaint.setColor(mExampleColor);
        mTextWidth = mTextPaint.measureText(mExampleString);

        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        mTextHeight = fontMetrics.bottom;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // TODO: consider storing these as member variables to reduce
        // allocations per draw cycle.
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

        // Draw the text.
        canvas.drawText(mExampleString,
                paddingLeft + (contentWidth - mTextWidth) / 2,
                paddingTop + (contentHeight + mTextHeight) / 2,
                mTextPaint);

        // Draw the example drawable on top of the text.
        if (mExampleDrawable != null) {
            mExampleDrawable.setBounds(paddingLeft, paddingTop,
                    paddingLeft + contentWidth, paddingTop + contentHeight);
            mExampleDrawable.draw(canvas);
        }
    }

    /**
     * Gets the example string attribute value.
     *
     * @return The example string attribute value.
     */
    public String getExampleString() {
        return mExampleString;
    }

    /**
     * Sets the view's example string attribute value. In the example view, this string
     * is the text to draw.
     *
     * @param exampleString The example string attribute value to use.
     */
    public void setExampleString(String exampleString) {
        mExampleString = exampleString;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * Gets the example color attribute value.
     *
     * @return The example color attribute value.
     */
    public int getExampleColor() {
        return mExampleColor;
    }

    /**
     * Sets the view's example color attribute value. In the example view, this color
     * is the font color.
     *
     * @param exampleColor The example color attribute value to use.
     */
    public void setExampleColor(int exampleColor) {
        mExampleColor = exampleColor;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * Gets the example dimension attribute value.
     *
     * @return The example dimension attribute value.
     */
    public float getExampleDimension() {
        return mExampleDimension;
    }

    /**
     * Sets the view's example dimension attribute value. In the example view, this dimension
     * is the font size.
     *
     * @param exampleDimension The example dimension attribute value to use.
     */
    public void setExampleDimension(float exampleDimension) {
        mExampleDimension = exampleDimension;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * Gets the example drawable attribute value.
     *
     * @return The example drawable attribute value.
     */
    public Drawable getExampleDrawable() {
        return mExampleDrawable;
    }

    /**
     * Sets the view's example drawable attribute value. In the example view, this drawable is
     * drawn above the text.
     *
     * @param exampleDrawable The example drawable attribute value to use.
     */
    public void setExampleDrawable(Drawable exampleDrawable) {
        mExampleDrawable = exampleDrawable;
    }

    private static class RotatingDrawable extends LayerDrawable {
        public RotatingDrawable(Drawable drawable) {
            super(new Drawable[] { drawable });
        }

        private float mRotation;

        public float getRotation() {
            return mRotation;
        }

        public void setRotation(float rotation) {
            mRotation = rotation;
            invalidateSelf();
        }

        @Override
        public void draw(Canvas canvas) {
            canvas.save();
            canvas.rotate(mRotation, getBounds().centerX(), getBounds().centerY());
            super.draw(canvas);
            canvas.restore();
        }
    }
}
